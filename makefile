# This makefile is intended to build and install the shared library
# to the default location. Tested on Fedora 18. 
#
# Please note that I am new to makefiles and I am sure this all could
# be done in better way. Feel free to let me know at vojta.havlicek[at]gmail.com
# so I can improve my coding ability for the next project I make. Thanks!
#
# Do you hear the buzz?

PROJECT=cicada

# Path to shared libraries in the system
LIB_PATH=/usr/lib

# Path to headers in the system
HDR_PATH=/usr/include

# Compiler
CC=g++

# Compiler C flags
# TODO: is there any reasonable way to check for these dependencies and
# eventually install them automatically?
#
OPT=-O2 -lglfw3 -lGL -lGLU -lX11 -lpthread -lXxf86vm -lm -lrt -lXrandr -lXi -std=C++11

# File list
OBJ=cicada.o

# Targets
.PHONY: lib
.PHONY: install
.PHONY: uninstal
.PHONY: clean

# Make everything from sratch
lib: $(PROJECT)

# Install to usr/lib - installs like app.
install: lib
	cp lib$(PROJECT).so -t $(LIB_PATH)
	cp $(PROJECT).h -t $(HDR_PATH)
	chmod 0755 $(LIB_PATH)/lib$(PROJECT).so
	ldconfig

# Uninstall from usr/lib
uninstall:
	rm -f $(HDR_PATH)/$(PROJECT).h
	rm -f $(LIB_PATH)/lib$(PROJECT).so
	ldconfig

# Cleans *.o
clean:
	rm -f *.o $(PROJECT)

# Creates the shared library file
$(PROJECT): $(OBJ)
	$(CC) -c -fpic cicada.cpp -o $(OBJ)
	$(CC) -shared -o lib$(PROJECT).so $(OBJ)











