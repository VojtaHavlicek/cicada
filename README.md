Cicada project
==============

The idea:
----------
Cicada is a simple bridge between C++ and GLFW 3 allowing for rapid programming of canvas-like apps - that simply because it's a great fun! I have grown up on AS3 and Flex SDK and
I still find this technology amazing. The idea is to blend the good things from the old Flash days with the awesomeness
C languages offer and create a featherlight framework to be creative with.

The execution:
---------
Cicada is prepared as a shared library for Linux, wrapping up GLFW 3.
When invoked from code the "canvas" created so far is a Flash style 550x400 without decorations. 
There are two function pointers passed to the library which link with the app. lifecycle. 
Use ESC to terminate the app.

Getting it to work:
------------------
1. Grab the source. Make the library with the following in bash
  ```bash
     make 
     sudo make install
  ```
This will build the library and install it to /usr/lib. Header is also installed to /usr/include. 

2. Feel creative. Use examples/main.cpp as your template and write some nice OpenGL stuff into redraw() and update() function bodies.

3. Check your dependencies and gcc. Make sure you install the awesome GLFW 3.0 library (directly from here: http://www.glfw.org/). Build with
   ```bash 
      g++ main.cpp -o main -lcicada -lglfw3 -lGL -lGLU -lX11 -lpthread -lXxf86vm -lm -lrt -lXrandr -lXi
      ```
    The linking options besides -lcicada are the GLFW 3.0 dependencies.

Does this all feel too noobish?
----------------------
I am happy to take any critics as I know my code is far from being perfect. Please help me to improve this by shouting at me at vojta.havlicek[at]gmail[dot]com. Any response appreciated.

Made with love far in the north. 



