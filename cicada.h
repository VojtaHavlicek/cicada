
/*-----------------------------------------------------------------------------
 *  cicada.h
 *
 *  INCLUDES:
 *-----------------------------------------------------------------------------*/
#define GLFW_INCLUDE_GLU

#pragma once

#include <GLFW/glfw3.h>    // Trying GLFW
// #include <GL/freeglut.h> // Include freeglut header
#include <iostream>
#include <cstdlib>
#include <cstdio>

namespace cicada
{
    /*-----------------------------------------------------------------------------
     *  VARS:
     *-----------------------------------------------------------------------------*/
    static int canvas_width  = 550;
    static int canvas_height = 400;

    static int xpos = 0; // window position on the screen
    static int ypos = 0;

	  GLFWmonitor* primary_monitor; // Primary monitor handler.
	  GLFWwindow*  app_window;      // Application window
    const GLFWvidmode* video_mode;      // Desktop video mode structure. .height + .width.

    void* update(void);		    // Function pointers to loop access 
    void* redraw(void);		    // callbacks

    bool verbose = true;		    // Trace out frequently?

    /*-----------------------------------------------------------------------------
     *  FUNCTIONS
     *-----------------------------------------------------------------------------*/
    void init(void (*cycle)(void), void(*redraw)(void)); // Initialize GLFW window


    void cycle(); 	              // Idle state of the window
    void die();			      // Terminate it all 

    // callbacks
    void error_callback(int error, const char* description); // GLFW error handler
    void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods); // GLFW key handler
}
