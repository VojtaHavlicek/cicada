/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  Cicada library example. Really far from being perfect :D
 *
 *        Version:  1.0
 *        Created:  09/07/13 17:52:45
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Vojtech Havlicek (Vh), vojta.havlicek@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include	<cicada.h>
#include	<stdlib.h>


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  update
 *  Description:  
 * =====================================================================================
 */
void update()
{
}

void redraw()
{
    glBegin(GL_TRIANGLES);
    glColor3f(0.1, 0.2, 0.3);
    glVertex2f(0, 0.58);
    glVertex2f(-0.5, -0.29);
    glVertex2f(0.5, -0.29);
    glEnd();
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */
    int
main ( int argc, char *argv[] )
{
    cicada::init(update, redraw);
    
    return EXIT_SUCCESS;
} /* ----------  end of function main  ---------- */















