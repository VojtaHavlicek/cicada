/*-----------------------------------------------------------------------------
 *  cicada.cpp
 *
 *  Contains implementation of cicada.h
 *-----------------------------------------------------------------------------*/

#include "cicada.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  init
 *  Description:  inits the GLFW window. Takes the implementation of redraw and cycle
 *  		  functions as an input.
 * =====================================================================================
 */
void cicada::init(void (*cycle)(void), void (*redraw)(void))
{
    // INIT GLFW
    glfwSetErrorCallback(error_callback); // error handler

    if(!glfwInit())
	exit(EXIT_FAILURE);

    std::cout << "cicada canvas inited!" << std::endl;

    // CREATE THE APPLICATION WINDOW 
    primary_monitor = glfwGetPrimaryMonitor(); 
    video_mode      = glfwGetVideoMode(primary_monitor);
    
    std::cout << "screen width: "   << video_mode->width 
              << " screen height: " << video_mode->height 
              << std::endl;
    
    glfwWindowHint(GLFW_DECORATED, GL_FALSE); // set the window hint undecorated
    app_window      = glfwCreateWindow(550, 400, "cicada", NULL, NULL); 
    
    if(!app_window)
    {
    	glfwTerminate();
    	exit(EXIT_FAILURE);
    }

    xpos = (video_mode->width - canvas_width) >> 1;   // center the window
    ypos = (video_mode->height - canvas_height) >> 1;

    glfwSetWindowPos(app_window,xpos,ypos); 
    glfwMakeContextCurrent(app_window);
    glfwSetKeyCallback(app_window, key_callback);

    // APPLICATION LIFECYCLE
    while(!glfwWindowShouldClose(app_window))
    {
    	cycle();
    	redraw();

    	glfwSwapBuffers(app_window);
	glfwPollEvents();
    }

    // HOUSEKEEPING
    glfwDestroyWindow(app_window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  error_callback
 *  Description:  GLFW error handler
 * =====================================================================================
 */
void cicada::error_callback(int error, const char* description)
{
	fputs(description, stderr); // using C-style output (from GLFW tut)
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  keyboard_callback
 *  Description:  GLFW keyboard callback. 
 *  		  Directly from: http://www.glfw.org/docs/3.0/quick.html
 * =====================================================================================
 */
void cicada::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
        if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
                    glfwSetWindowShouldClose(window, GL_TRUE);
}








